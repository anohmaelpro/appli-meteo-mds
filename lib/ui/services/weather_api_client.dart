import 'package:MyWeather/ui/models/weather_model.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class WeatherApiClient {
  Future<WeatherModel> getCurrentWeather(String? location) async{
    var endpoint = Uri.parse("https://api.openweathermap.org/data/2.5/weather?q=$location&appid=b42f5ef2dc0418a623cd7963ebf8fbcc&units=metric");

    var response = await http.get(endpoint);
    var body = convert.jsonDecode(response.body);

    WeatherModel weather = WeatherModel.fromJsonApi(body);

    if (kDebugMode) {
      print ('json info');
      print (body) ;
      print ('json info done');
    }


    return weather;
  }
}