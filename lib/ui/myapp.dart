import 'package:MyWeather/ui/screens/home.dart';
import 'package:MyWeather/ui/screens/weather.dart';
import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title : 'WeatherApp',
      theme: ThemeData(
          primarySwatch: Colors.blue
      ),
      routes: {
        "/city_weather" : (context) => const Weather(),
        "/home" : (context) => Home(),
      },
      initialRoute: "/home",
      // home: Home(),
    );
  }
}
