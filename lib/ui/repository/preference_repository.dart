import 'dart:convert';
import 'package:MyWeather/ui/models/weather_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {

  Future<void> saveCityWeather(List<WeatherModel> cityWeather) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final List<String> listJson = [];
    for (final WeatherModel weather in cityWeather) {
      listJson.add(weather.toJson());
    }

    prefs.setStringList('cityWeather', listJson);
  }

  Future<List<WeatherModel>> loadCityWeather() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String>? list = prefs.getStringList('cityWeather');

    if(list == null) {
      return [];
    }

    final List<WeatherModel> cityWeather = list.map((element) {
      return WeatherModel.fromJsonPreference(jsonDecode(element));
    }).toList();

    return cityWeather;
  }
}