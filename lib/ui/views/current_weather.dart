import 'package:flutter/material.dart';

Widget currentWeather(String icon, String temp, String location){
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.network(
          "https://openweathermap.org/img/wn/${icon}@2x.png",
          height: 200.0,
          width: 200.0,
          fit: BoxFit.fill,
        ),
        const SizedBox(
          height: 10.0,
        ),
        Text(
            temp,
          style: const TextStyle(
            fontSize: 66.0,
          ),
        ),
        const SizedBox(
          height: 10.0,
        ),
        Text(
          location,
          style: const TextStyle(
            fontSize: 25.0,
            color: Color(0xFF5a5a5a)
          ),
        ),
      ]),
  );
}