
import 'package:MyWeather/ui/models/weather_model.dart';
import 'package:MyWeather/ui/services/weather_api_client.dart';
import 'package:MyWeather/ui/views/additional_information.dart';
import 'package:MyWeather/ui/views/current_weather.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Weather extends StatefulWidget {
  const Weather({Key? key}) : super(key: key);
  @override
  _MeteoState createState() => _MeteoState();
}

class _MeteoState extends State<Weather> {


  final String _myCityName = "";

  WeatherApiClient clientCallApiWeather = WeatherApiClient();
  WeatherModel? weatherCityDataFromApi = WeatherModel();


  Future<WeatherModel?> getData() async {
    String? cityName =  ModalRoute.of(context)?.settings.arguments as String?;
    if (cityName.toString().length <= 2){
      return null;
    }
    else {
      weatherCityDataFromApi = await clientCallApiWeather.getCurrentWeather(cityName);
      return weatherCityDataFromApi;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFf9f9f9),
      appBar: AppBar(
        backgroundColor: const Color(0xFFf9f9f9),
        title: const Text("Weather", style: TextStyle(color: Colors.black)),
        elevation: 0.0,
        centerTitle: true ,
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          icon: const Icon(Icons.arrow_back),
          color: Colors.black,
        ),
      ),
      body: FutureBuilder(
        future: getData(),
        builder: (context, snapshot){
          if (snapshot.connectionState == ConnectionState.done){
            // when no data found by the Api
            if (weatherCityDataFromApi!.temp == null){
              return AlertDialog(
                title: const Text('Popup example'),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const <Widget>[
                    Text("😭 Sorry no data weather found for this city 😭", textAlign: TextAlign.center,),
                    Text("😉 Please make sure that you wrote well the city name 😉", textAlign: TextAlign.center,),
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    textColor: Theme.of(context).primaryColor,
                    child: const Text('Close'),
                  ),
                ],
              );
            }

            // when everything went well, i mean we the data got from the Api and the json transformation
            else {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  currentWeather("${weatherCityDataFromApi!.icon}", "${(weatherCityDataFromApi!.temp)!.round()}°", "${weatherCityDataFromApi!.cityName}"),
                  const SizedBox(
                    height: 60.0,
                  ),
                  const Text(
                      "More Information",
                    style: TextStyle(
                      fontSize: 24.0,
                      color: Color(0xdd212121),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const Divider(),
                  const SizedBox(height: 20.0),
                  additionalInformation(
                      weatherCityDataFromApi!.wind.toString(),
                      weatherCityDataFromApi!.humidity.toString(),
                      weatherCityDataFromApi!.pressure.toString(),
                      weatherCityDataFromApi!.feels_like.toString()
                  ),
                  TextButton(
                      onPressed: (){
                        // TODO : return the data to home page
                        WeatherModel weatherCityData = weatherCityDataFromApi as WeatherModel ;
                        if (kDebugMode) {
                          print(weatherCityData);
                        }
                        Navigator.of(context).pop(weatherCityData);
                      },
                      child: const Text("Save this city"))
                ],
              );
            }
          }
          //  while getting data from the api we start a loader component
          else if (snapshot.connectionState == ConnectionState.waiting){
            return const Center(child: CircularProgressIndicator());
          }

          // the case when everything goes bad
          else {
            return AlertDialog(
              title: const Text('Oups No Data Found !'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const <Widget>[
                  Text("😭 Sorry no data weather found for this city 😭", textAlign: TextAlign.center,),
                  Text("😉 Please make sure that you wrote well the city name 😉", textAlign: TextAlign.center,),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  textColor: Theme.of(context).primaryColor,
                  child: const Text('Close'),
                ),
              ],
            );
          }
        },
      ),
    );

  }
}
