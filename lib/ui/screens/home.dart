

import 'package:MyWeather/ui/models/weather_model.dart';
import 'package:MyWeather/ui/repository/preference_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  final PreferencesRepository preferencesRepository = PreferencesRepository();

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String citySearched = "";
  bool isExist =  false;

  List<WeatherModel> _weathers = [];

  @override
  void initState() {
    widget.preferencesRepository.loadCityWeather().then((cityWeather) {
      _setCityWeather(cityWeather);
    });
    super.initState();
  }

  void _setCityWeather(List<WeatherModel> cityWeather) {
    setState(() {
      _weathers = cityWeather;
    });
  }


  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Cities Weather"),
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 0.5),
        child: Column(
          children: [
            TextFormField(
              controller: _textEditingController,
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.location_on),
                labelText: "city name",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10)
                ),
              ),
              onChanged: (value){
                if (kDebugMode) {
                  print(value);
                }
                if (value.length > 2){
                  citySearched =  value ;
                }
              },
            ),
            TextButton(
                onPressed: () async {
                  print("ville rechercher :  ${citySearched}") ;
                  final WeatherModel? weatherFav = await Navigator.of(context).pushNamed("/city_weather", arguments: citySearched) as WeatherModel;
                  if (kDebugMode) {
                    print(weatherFav);
                  }

                  if (weatherFav != null){
                    if (kDebugMode) {
                      print (weatherFav.cityName);
                    }
                    int i = 0 ;
                    while((isExist == false) && (i < _weathers.length)){
                      for (WeatherModel weather in _weathers){
                        if (weather.cityName == weatherFav.cityName){
                          isExist = true ;
                        }
                        else {
                          isExist = false ;
                        }
                      }
                      i++;
                    }

                    if (isExist == false){
                      setState(() {
                        _weathers.add(weatherFav);
                      });
                      widget.preferencesRepository.saveCityWeather(_weathers);
                      isExist = false ;
                    }else {
                      isExist = false ;
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Vous avez déjà ajouter la ville ${weatherFav.cityName}"),
                      ));
                    }

                  }
                },
                child: Text("Validate")),
            const Center(
              heightFactor: 5,
              child: Text("Mes Enregistrés"),
            ),
            Expanded(
                child:ListView.builder(
                  itemCount: _weathers.length,
                  itemBuilder: (context, index){
                    WeatherModel weather = _weathers[index];
                    return ListTile(
                      // style: ,
                      title: Text((weather.cityName.toString()).toUpperCase()),
                      leading: Image.network(
                        "https://openweathermap.org/img/wn/${weather.icon}@2x.png",
                      ),
                      onTap: (){
                        Navigator.of(context).pushNamed("/city_weather", arguments: weather.cityName);
                      },
                      onLongPress: (){
                        setState(() {
                          _weathers.remove(weather);
                        });

                        widget.preferencesRepository.saveCityWeather(_weathers);
                      },
                    );
                  }
                )
            )
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
