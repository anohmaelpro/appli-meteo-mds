import 'dart:convert';

class WeatherModel {
  String? cityName;
  double? temp;
  double? wind;
  int? humidity;
  double? feels_like;
  int? pressure;
  String? icon;



  WeatherModel({
    this.cityName,
    this.temp,
    this.feels_like,
    this.humidity,
    this.wind,
    this.pressure,
    this.icon,
  });

  WeatherModel.fromJsonApi(Map<String, dynamic> json){
    cityName = json["name"];
    temp = json["main"]["temp"];
    feels_like = json["main"]["feels_like"];
    pressure = json["main"]["pressure"];
    humidity = json["main"]["humidity"];
    wind = json["wind"]["speed"];
    icon = json["weather"][0]["icon"];
  }

  String toJson(){
    return jsonEncode({
      "cityname": cityName,
      "temp": temp,
      "feels_like": feels_like,
      "humidity": humidity,
      "wind": wind,
      "pressure": pressure,
      "icon": icon,
    });
  }


  WeatherModel.fromJsonPreference(Map<String, dynamic> json){
    cityName = json["cityname"];
    temp = json["temp"];
    feels_like = json["feels_like"];
    pressure = json["pressure"];
    humidity = json["humidity"];
    wind = json["wind"];
    icon = json["icon"];
  }




}