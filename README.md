# appli_meteo


Afin de réaliser le projet de cours sur le module Flutter, j'ai réalisé une application météo
Je me sers d'une api meteo "https://api.openweathermap.org/data/2.5/weather?q=$location&appid", dans la liste des apis qui nous a été donné.

Page Home : 
Cette page contient une barre de recherche qui permet d'avoir la météo de la ville saisie.
Ensuite plus bas "mes enrégistrés" qui contient la liste des villes dont on souhaite enrégistré.

Au clic rapide on accède au donnée météo en directe.
Au long press on supprime l'élément presser de la liste des favoris.

Page Météo : 
Après avoir saisie le ville dont on souhaite avoir la météo en cliquant sur "Validate",
on accède à cette page. Elle donne les informations météorologique de la ville saisie.





## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
